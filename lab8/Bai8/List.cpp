#include "List.h"

List::List() {
	count = 0;
	pHead = NULL;
}

List::List(const List& x) {
	this->pHead = new Node(x.pHead->data);
	Node* pTemp1 = this->pHead;
	Node* pTemp2 = x.pHead->next;
	while (pTemp2) {
		Node* pVal = new Node(pTemp2->data);
		pTemp1->next = pVal;
		pTemp1 = pTemp1->next;
		pTemp2 = pTemp2->next;
	}
	this->count = x.count;
}

List::~List() {
	count = 0;
	Node*pTemp = this->pHead;
	while (pHead) {
		pHead = pHead->next;
		delete pTemp;
		pTemp = pHead;
	}
}

std::ostream& operator<<(std::ostream& os, const List& x) {
	Node* pTemp = x.pHead;
	while (pTemp) {
		os << pTemp->data << " ";
		pTemp = pTemp->next;
	}
	return os;
}

std::istream& operator>>(std::istream& is, List& x) {
	int data;
	is >> data;
	x.pHead = new Node(data);
	x.count = 1;
	Node* pTemp = x.pHead;
	while (!is.eof()) {
		is >> data;
		Node* pVal = new Node(data);
		pTemp->next = pVal;
		pTemp = pTemp->next;
		x.count++;
	}
	return is;
}

bool List::operator==(const List& x) {
	int i = 0;
	Node* pTemp1 = this->pHead;
	Node* pTemp2 = x.pHead;
	while (pTemp1 && pTemp2) {
		if (pTemp1->data == pTemp2->data) {
			i++;
		}
		pTemp1 = pTemp1->next;
		pTemp2 = pTemp2->next;
	}
	if (i == this->count && i == x.count) {
		return true;
	}
	return false;
}

List& List::operator=(List x) {
	//swap this.count and x.count
	int tempN = this->count;
	this->count = x.count;
	x.count = tempN;
	//swap this->pHead and x.pHead;
	Node* pTemp = this->pHead;
	this->pHead = x.pHead;
	x.pHead = pTemp;
	return *this;
}

List List::operator+(List x) {
	List temp(*this);
	Node* pTemp = temp.pHead;
	while (pTemp->next) {
		pTemp = pTemp->next;
	}
	pTemp->next = x.pHead;
	temp.count += x.count;
	x.pHead = NULL;
	return temp;
}

bool List::readList(char* filename) {
	fstream f(filename, ios::in);
	if (!f) {
		cout << "Khong doc duoc file" << endl;
		return false;
	}
	else {
		f >> *this;
		f.close();
	}
	return true;
}

bool List::writeList(char* filename) {
	fstream f(filename, ios::out);
	if (this->pHead == NULL) {
		cout << "Khong ghi duoc file" << endl;
		return false;
	}
	else {
		f << *this;
		f.close();
	}
	return true;
}

int List::reverse() {
	Node* prev = NULL;
	Node* next = NULL;
	Node* current = this->pHead;
	while (current) {
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	this->pHead = prev;
	return 1;
}

void List::deleteNode(int x) {
	Node* pTemp = pHead;
	Node* pPre = NULL;
	if (pTemp->data == x) {
		pHead = pTemp->next;
		delete pTemp;
	}
	else {
		while (pTemp->next) {
			pPre = pTemp;
			pTemp = pTemp->next;
			if (pTemp->data == x) {
				pPre->next = pTemp->next;
				delete pTemp;
				break;
			}
		}
	}
	count--;
}

int List::deleteOddNode() {
	Node* curr = pHead;
	Node* pre = NULL;
	while (curr != NULL && curr->data % 2 != 0) {
		curr = curr->next;
		count--;
	}
	pHead = curr;
	if (curr == NULL) {
		return 1;
	}
	pre = curr;
	curr = curr->next;
	while (curr != NULL) {
		if (curr->data % 2 == 0) {
			pre->next = curr;
			pre = curr;
		}
		else {
			count--;
		}
		curr = curr->next;
	}
	pre->next = NULL;
	return 1;
}

int List::minPos() {
	if (pHead == NULL) {
		return -1;
	}
	Node* p = pHead->next;
	int i = 1, k = i, x = pHead->data;
	for (i; i < (*this).count; i++) {
		if (p->data < x) {
			x = p->data;
			k = i;
		}
		p = p->next;
	}
	return k;
}

bool List::split(int pos, List& list) {
	if (pos < 0 || pos >= count) {
		return false;
	}
	Node* curr = pHead;
	Node* pre = NULL;
	for (int i = 0; i < pos; i++) {
		pre = curr;
		curr = curr->next;
	}
	list.count = (*this).count - pos;
	(*this).count = pos;
	pre->next = NULL;
	list.pHead = curr;
	return true;
}

void List::insertOrderedList(int n) {
	Node* newN = new Node(n);
	if (newN->data <= pHead->data) {
		newN->next = pHead;
		pHead = newN;
		count++;
		return;
	}
	Node* curr = pHead;
	Node* pre = NULL;
	while (curr->next) {
		pre = curr;
		curr = curr->next;
		if (newN->data <= curr->data) {
			pre->next = newN;
			newN->next = curr;
			pre = newN;
			count++;
			return;
		}
	}
	if (newN->data >= curr->data) {
		curr->next = newN;
		count++;
	}
}

void List::printReverseList(Node* head) {
	if (head->next == NULL) {
		cout << head->data << " ";
		return;
	}
	printReverseList(head->next);
	cout << head->data << " ";
}

void List::printList(Node* head) {
	if (head) {
		cout << head->data << " ";
		printList(head->next);
	}

}
