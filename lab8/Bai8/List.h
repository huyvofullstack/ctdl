#pragma once
#include <iostream>
#include <fstream>
using namespace std;

class Node {
public:
	int data;
	Node* next;
	Node() {
		data = 0;
		next = NULL;
	}

	Node(int data) {
		this->data = data;
		this->next = NULL;
	}
};

class List {
//private:
//	int count;
//	Node* pHead;
public:
	int count;
	Node* pHead;
	List();
	List(const List& x);
	~List();
	friend ostream& operator<<(ostream& os, const List& x);
	friend istream& operator>>(istream& is, List& x);
	bool operator==(const List& x);
	List& operator=(List x);
	List operator+(List x);
	bool readList(char* filename);
	bool writeList(char* filename);
	int reverse();
	void deleteNode(int x);
	int deleteOddNode(); // xoa phan tu le
	int minPos();
	int getCount() {
		return this->count;
	}
	bool split(int pos, List &list);
	void insertOrderedList(int n);
	void printReverseList(Node* head);
	void printList(Node* head);
};
