#include "List.h"

//int main() {
//	List a, b;
//	a.readList("a.txt");
//	/*a.insertOrderedList(6);
//	cout << a << endl;*/
//	/*a.writeList("out_a.txt");
//	b.readList("b.txt");
//	b.writeList("out_b.txt");
//	cout << a << endl << b << endl << (a == b) << endl;
//	List c = a + b;
//	cout << c << endl;
//	a.reverse();*/
//	//cout << a << endl;
//	//a.deleteNode(1);
//	//a.deleteOddNode();
//	//cout << a << endl;
//	//cout << a.getCount() << endl;
//	//cout << a.minPos() << endl;
//	/*a.split(2, b);
//	cout << a << endl << b << endl;*/
//	cout << a << endl;
//	a.printReverseList(a.pHead);
//	a.printList(a.pHead);
//	return 0;
//}

void link(List& a, List b) {
	Node* p = a.pHead;
	while (p->next) {
		p = p->next;
	}
	p->next = b.pHead;
	a.count = a.count + b.count;
	b.pHead = NULL;
}

Node *trim(Node* head) {
	if (head == NULL) {
		return NULL;
	}
	head->next = trim(head->next);
	if (head->next == NULL) {
		if (head->data == 0) {
			free(head);
			return NULL;
		}
		else {
			return head;
		}
	}
	return head;
}

int main() {
	List a;
	a.pHead = new Node(1);
	a.pHead->next = new Node(2);
	a.pHead->next->next = new Node(0);
	a.pHead->next->next->next = new Node(0);
	a.pHead->next->next->next->next = new Node(0);
	a.pHead->next->next->next->next->next = new Node(0);
	a.pHead = trim(a.pHead);
	cout << a << endl;

	/*List b;
	b.pHead = new Node(4);
	b.pHead->next = new Node(5);
	b.pHead->next->next = new Node(6);

	link(a, b);
	
	cout << a << endl;
	cout << a << endl;*/
}