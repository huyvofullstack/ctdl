// ListTraverseSample.cpp : Defines the entry point for the console application.
//


enum Error_code {success,fail,rangeerror,overflow,not_present,
				dupplicated,found};
//---------------------------------------------------------
template<class Node_entry> 
struct Node {
	Node_entry entry;
	Node<Node_entry> *next;
	Node();
	Node(Node_entry item, Node<Node_entry> * link = NULL);
};
//---------------------------------------------------------
template <class List_entry>
class List  {
public:
	void clear();
	List();
	~List();
	
	bool empty() ;
	int size() ;
	Error_code insert(int position,   List_entry x);
	void printAll();
//	Error_code remove(int position, List_entry &x);
//	Error_code retrieve(int position, List_entry &x) ;

private:
	int count;
	Node<List_entry> *head;
	Node<List_entry> *set_position(int position) ;
};
//---------------------------------------------------------
template<class Node_entry>
Node<Node_entry>::Node() {
	next = NULL;
}
//---------------------------------------------------------
template<class Node_entry>
Node<Node_entry>::Node(Node_entry item,Node<Node_entry> * link) {
	entry = item;
	next = link;
}
//---------------------------------------------------------
template<class List_entry>
List<List_entry>::List() {
	head = NULL;
	count = 0;
}
//---------------------------------------------------------
template<class List_entry>
List<List_entry>::~List() {
	List_entry temp;
	while(!empty())
	{
		remove(0,temp);
	}
}
//---------------------------------------------------------
template<class List_entry>
Node<List_entry> * List<List_entry>::set_position(int position) {
	Node<List_entry> *q = head;
	for(int i=0;i<position;i++) q = q->next;
	return q;
}
//---------------------------------------------------------
template<class List_entry>
void List<List_entry>::clear() {
	List_entry temp;
	while(!empty())
		remove(0,temp);
}
//---------------------------------------------------------
template<class List_entry>
int List<List_entry>::size() {
	return count;
}
//---------------------------------------------------------
template<class List_entry>
Error_code List<List_entry>::insert(int position, List_entry x) {
	if(position<0 || position>count)
	{return rangeerror;}
	Node<List_entry> *new_node, *previous, *following;
	if(position>0) {
			previous = set_position(position-1);
			following = previous->next;
	} else {
		following = head;
	}
	new_node = new Node<List_entry>(x,following);
	if(new_node ==NULL)
		return overflow;
	if(position == 0)
		head = new_node;
	else
		previous->next = new_node;
	count++;
	return success;
}
//---------------------------------------------------------
/*template<class List_entry>
Error_code List<List_entry>::remove(int position, List_entry &x)
{
}*/

//---------------------------------------------------------
template<class List_entry>
bool List<List_entry>::empty() {
	return (count==0);
}

//---------------------------------------------------------
/*template<class List_entry>
Error_code List<List_entry>::retrieve(int position, List_entry &x) 
{
}*/

//---------------------------------------------------------
/*template <class List_entry>
void List<List_entry>::printAll()
{
}*/
