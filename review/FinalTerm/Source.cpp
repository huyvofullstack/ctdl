#include <iostream>
using namespace std;

struct Node {
	int entry;
	Node* next;
	
	Node() {
		entry = 0;
		next = NULL;
	}

	Node(int entry, Node* next) {
		this->entry = entry;
		this->next = next;
	}
};

class LinkedList {
public:
	Node* head;
	int count;
public:
	LinkedList() {
		count = 0;
		head = NULL;
	}

	~LinkedList() {
		if (head != NULL) {
			Node* temp = NULL;
			while (head->next) {
				temp = head;
				head = head->next;
				delete(temp);
			}
			delete(head);
			count = 0;
		}
	}

	bool remove(int pos) {
		if (head == NULL || pos < 0 || pos >= count) {
			return false;
		}
		else {
			if (pos == 0) {
				Node* temp = head;
				head = head->next;
				delete(temp);
			}
			else {
				Node* pre = NULL;
				Node* curr = head;
				for (int i = 0; i < pos; i++) {
					pre = curr;
					curr = curr->next;
				}
				pre->next = curr->next;
				delete(curr);
			}
			count--;
			return true;
		}
	}

};
// 1 - 2 - 0 - 2 - 0 - 0
Node* trimList(Node* head) {
	if (head == NULL) {
		return NULL;
	}
	head->next = trimList(head->next);
	if (head->next == NULL) {
		if (head->entry == 0) {
			free(head);
			return NULL;
		}
		else {
			return head;
		}
	}
	return head;
}

int main() {
	LinkedList a ;
	a.head = new Node(1, new Node(2, new Node(0, new Node(2, (new Node(0, (new Node(0, NULL))))))));
	/*a.head = new Node(0, NULL);*/
	a.count = 4;
	/*a.remove(2);
	Node* temp = a.head;
	while (temp) {
		cout << temp->entry << " ";
		temp = temp->next;
	}*/
	a.head = trimList(a.head);
	Node* temp = a.head;
	while (temp) {
		cout << temp->entry << " ";
		temp = temp->next; 
	}
	return 0;
}

/*
Algorithm ReverseStack(Stack S)
	1. Queue Q;
	2. while (!S.isEmpty())
		2.1 S.popStack(x, S);
		2.2 Q.enQueue(x, Q)
	3. while (!Q.isEmpty())
		3.1 Q.deQueue(x, Q)
		3.2 S.pushStack(x, S)
End
*/