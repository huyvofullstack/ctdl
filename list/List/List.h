#include <iostream>

class Node {
public:
	int data;
	Node* next;
	Node(int data) {
		this->data = data;
		this->next = NULL;
	}
};

class List {
	int count;
	Node* pHead;
public:
	List();
	void addLast(int data);
	void printAll();
	bool remove(int pos);
	int findMax();
	int findMin();
	bool insert(int pos, int data);
	int firstIndexOf(int data);
	int lastIndexOf(int data);
	int minPos();
	~List();
};

