#include "List.h"
#include <iostream>

List::List() {
	pHead = NULL;
	count = 0;
}

List::~List() {
	Node* pTemp = pHead;
	while (pTemp) {
		pHead = pHead->next;
		delete pTemp;
		pTemp = pHead;
	}
	count = 0;
}

void List::addLast(int data) {
	Node* pPos = new Node(data);
	if (pHead == NULL) {
		pHead = pPos;
	}
	else {
		Node* pTemp = pHead;
		while (pTemp->next) {
			pTemp = pTemp->next;
		}
		pTemp->next = pPos;
	}
	count++;
}

void List::printAll() {
	Node* pTemp = pHead;
	while (pTemp) {
		std::cout << pTemp->data << " ";
		pTemp = pTemp->next;
	}
	std::cout << std::endl;
}
