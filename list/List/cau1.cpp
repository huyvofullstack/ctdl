#include "List.h"
#include <iostream>
using namespace std;

bool List::remove(int pos) {
	if (pos < 0 || pos >= count) {
		return false;
	}
	Node* pPos = pHead;
	Node* pPre = NULL;
	for (int i = 0; i < pos; i++) {
		pPre = pPos;
		pPos = pPos->next;
	}
	if (pPre == NULL) {
		pHead = pHead->next;
	}
	else {
		pPre->next = pPos->next;
	}
	delete pPos;
	count--;
	return true;
}

bool List::insert(int pos, int data) {
	if (pos < 0 || pos > count) {
		return false;
	}
	Node* pPre = pHead;
	Node* pPos = new Node(data);
	if (pos == 0) {
		pPos->next = pHead;
		pHead = pPos;
	}
	else {
		for (int i = 0; i < pos - 1; i++) {
			pPre = pPre->next;
		}
		pPos->next = pPre->next;
		pPre->next = pPos;
	}
	count++;
	return true;;
}

int List::lastIndexOf(int data) {
	Node* pTemp = pHead;
	int i = 0;
	int k = -1;
	while (pTemp) {
		if (pTemp->data == data) {
			k = i;
		}
		i++;
		pTemp = pTemp->next;
	}
	return k;
}

int List::minPos() {
	if (pHead == NULL) {
		return -1;
	}
	Node* p = pHead->next;
	int i = 0, k = -1, x = p->data;
	while (p) {
		i++;
		if (p->data < x) {
			x = p->data;
			k = i;
		}
		p = p->next;
	}
	return k;
}