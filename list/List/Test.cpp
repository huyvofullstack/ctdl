#include "List.h"
#include <iostream>
using namespace std;

int main() {
	List list;
	list.addLast(1);
	list.addLast(2);
	list.addLast(1);
	list.addLast(3);
	list.addLast(4);
	list.addLast(2);
	cout << list.minPos() << endl;
	list.printAll();
	/*list.remove(4);
	list.remove(0);
	cout << "Max = " << list.findMax() << endl << "Min = " << list.findMin() << endl;*/
	//list.insert(0, 100);
	//list.printAll();
	//cout << list.firstIndexOf(10) << endl;
	//cout << list.lastIndexOf(0) << endl;
	return 0;
}