#include "List.h"
#include <iostream>
using namespace std;

int List::findMax() {
	if (pHead == NULL) {
		return -1;
	}
	int max = pHead->data;
	Node* pTemp = pHead->next;
	while (pTemp) {
		if (max < pTemp->data) {
			max = pTemp->data;
		}
		pTemp = pTemp->next;
	}
	return max;
}

int List::firstIndexOf(int data) {
	Node* pTemp = pHead;
	int i = 0;
	while (pTemp) {
		if (pTemp->data == data) {
			return i;
		}
		i++;
		pTemp = pTemp->next;
	}
	return -1;
}

int List::findMin() {
	if (pHead == NULL) {
		return -1;
	}
	int min = pHead->data;
	Node* pTemp = pHead->next;
	while (pTemp) {
		if (min > pTemp->data) {
			min = pTemp->data;
		}
		pTemp = pTemp->next;
	}
	return min;
}
