#include "BST.h"

BST::BST() {
	head = new Node();
}

BST::~BST() {
	if (head != NULL) {
		deleteRoot(head);
		head = NULL;
	}
}

int BST::countLeaf(Node* root) {
	if (root == NULL) {
		return 0;
	}
	if (root->left == NULL && root->right == NULL) {
		return 1;
	}
	else {
		return countLeaf(root->left) + countLeaf(root->right);
	}
}

void BST::printLNR(Node* root) {
	if (root != NULL) {
		printLNR(root->left);
		cout << root->data << " ";
		printLNR(root->right);
	}
}

int BST::countNode(Node* root) {
	if (root != NULL) {
		return 1 + countNode(root->left) + countNode(root->right);
	}
	return 0;
}

void BST::printEvenLRN(Node* root) {
	if (root != NULL) {
		printEvenLRN(root->right);
		printEvenLRN(root->left);
		if (root->data % 2 == 0) {
			cout << root->data << " ";
		}
	}
}

void BST::printLeaf(Node* root) {
	if (root == NULL) {
		return;
	}
	if (root->left == NULL && root->right == NULL) {
		cout << root->data << " ";
	}
	else {
		printLeaf(root->left);
		printLeaf(root->right);
	}
}

int BST::height(Node* root) {
	if (root == NULL) {
		return 0;
	}
	int m1 = height(root->left);
	int m2 = height(root->right);
	int max = m1 > m2? m1 : m2;
	return 1 + max;
}

// if root = null return 0
// else
// m1 = height(root->left)
// m2 = height(root->right)
//return 1 + max(m1, m2);

/*
Print the node at level i of BST
*/
void BST::printAtLevel(Node* root, int i) {
	if (root == NULL) {
		i++;
		return;
	}
	if (i == 0) {
		cout << root->data << " ";
		return;
	}
	printAtLevel(root->left, --i);
	i++;
	printAtLevel(root->right, --i);
}

// Do not try! There is something wrong with this function and I'm currently can't fix it.
int BST::find_K_Lowest(Node* root, int k, int m) {
	if (root == NULL || m >= k) {
		return 0;
	}
	m += find_K_Lowest(root->left, k, m); // This is where it gone wrong.
	if (m < k) {
		cout << root->data << " ";
		m += 1;
	}
	m += find_K_Lowest(root->right, k, m);
	return m;
}

int main() {
	BST bst;

	bst.head->data = 10;
	bst.head->left = new Node(5);
	bst.head->left->left = new Node(1);
	bst.head->left->left->right = new Node(4);
	bst.head->left->left->right->left = new Node(2);
	bst.head->left->right = new Node(8);
	bst.head->left->right->left = new Node(6);
	bst.head->left->right->right = new Node(9);

	bst.head->right = new Node(15);
	bst.head->right->left = new Node(12);
	bst.head->right->right = new Node(16);

	bst.printLNR(bst.head);

	//cout << endl << bst.countLeaf(bst.head) << endl;
	//cout << bst.countNode(bst.head) << endl;

	//bst.printEvenLRN(bst.head);

	//cout << endl;
	//bst.printLeaf(bst.head);

	//cout << endl;
	//cout << bst.height(bst.head) << endl;

	cout << endl;
	//bst.printAtLevel(bst.head, 2);
	bst.find_K_Lowest(bst.head, 3, 0);

	return 0;
}
