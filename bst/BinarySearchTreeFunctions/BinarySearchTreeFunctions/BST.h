#pragma once
#include <iostream>
using namespace std;

struct Node {
	int data;
	Node* left;
	Node* right;

	Node() {
		data = NULL;
		left = right = NULL;
	}
	
	Node(int data) {
		this->data = data;
		left = right = NULL;
	}
};

void deleteRoot(Node* head) {
	if (head == NULL) {
		return;
	}
	deleteRoot(head->left);
	deleteRoot(head->right);
	delete(head);
}

class BST {
public:
	Node* head;
public:
	BST();
	~BST();
	int countLeaf(Node* root);
	void printLNR(Node* root);
	int countNode(Node* root);
	void printEvenLRN(Node* root);
	void printLeaf(Node* root);
	int height(Node* root);
	void printAtLevel(Node* root, int i);
	int find_K_Lowest(Node* root, int k, int m);
};

