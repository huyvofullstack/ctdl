#ifndef _common_h_
#include "common.h"
#define _common_h_
#endif
#include <time.h>
#include <stack>
#include <Windows.h>
const int radix = 10;
char	operatorArr[MAX_OPERAND];
char	operandArr[MAX_OPERAND][MAX_DIGIT];
const int NUMBER_OF_TESTCASE = 104;
int countPass = 0; int countFail = 0;
stack<int> rStack, fStack;
/* A utility function to reverse a string  */
void reverse(char str[], int length)
{
	int start = 0;
	int end = length - 1;
	while (start < end)
	{
		swap(*(str + start), *(str + end));
		start++;
		end--;
	}
}
// Implementation of itoa()
char* itoa_Vinh(int num, char* str, int base, int in_or_out)
{
	int i = 0;
	bool isNegative = false;
	/* Handle 0 explicitely, otherwise empty string is printed for 0 */
	if (num == 0)
	{
		str[i++] = '0';
		str[i] = '\0';
		return str;
	}
	// In standard itoa(), negative numbers are handled only with 
	// base 10. Otherwise numbers are considered unsigned.
	if (num < 0 && base == 10)
	{
		isNegative = true;
		num = -num;
	}
	//file extension is '.txt'
	str[i++] = 't';
	str[i++] = 'x';
	str[i++] = 't';
	str[i++] = '.';
	// Process individual digits
	while (num != 0)
	{
		int rem = num % base;
		str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
		num = num / base;
	}
	// If number is negative, append '-'
	if (isNegative)
		str[i++] = '-';
	if (in_or_out == 1)
	{
		str[i++] = '/';
		str[i++] = 't';
		str[i++] = 'u';
		str[i++] = 'p';
		str[i++] = 'n';
		str[i++] = 'i';
	}
	if (in_or_out == 2)
	{
		str[i++] = '/';
		str[i++] = 't';
		str[i++] = 'u';
		str[i++] = 'p';
		str[i++] = 't';
		str[i++] = 'u';
		str[i++] = 'o';
	}
	str[i] = '\0'; // Append string terminator
				   // Reverse the string
	reverse(str, i);
	return str;
}
void writeFileRecur(FILE* f, digitList* head)
{
	if (head == NULL)	return;
	writeFileRecur(f, head->nextDigit);
	char	str[4];
	sprintf(str, "%d", head->digit);
	fwrite(str, 1, sizeof(char), f);
}
void writeFile(char* filename, Integer L)
{
	char* file_name = filename;
	FILE* f = 0;
	f = fopen(file_name, "w");
	if (f == NULL)	//file not found || cannot read
		return;
	if (L.sign < 0)
	{
		char	str[80];
		strcpy(str, "-");
		fwrite(str, 1, sizeof(char), f);
	}
	writeFileRecur(f, L.digits);
	fclose(f);
}
int readFile(char* filename, int&	operandNum, int& operatorNum)
{
#define MAX_CHARACTER_PER_LINE 80
	char* file_name = filename;
	FILE* f = 0;
	f = fopen(file_name, "r");
	if (f == NULL)	//file not found || cannot read
		return 0;
	///////////////////////////////////////////////
	char	str[MAX_CHARACTER_PER_LINE];
	fgets(str, MAX_CHARACTER_PER_LINE, f);
	sscanf(str, "%d", &operandNum);
	operatorNum = operandNum - 1;
	int i;
	for (i = 0; i<operandNum; i++)
	{
		fgets(str, MAX_CHARACTER_PER_LINE, f);
		strcpy(operandArr[i], str);
		for (int j = 0; j < strlen(operandArr[i]); j++)
			if (operandArr[i][j] == '\r' || operandArr[i][j] == '\n')
			{
				operandArr[i][j] = '\0';
				break;
			}
	}
	for (i = 0; i<operatorNum; i++)
	{
		fgets(str, MAX_CHARACTER_PER_LINE, f);
		operatorArr[i] = str[0];
	}
	///////////////////////////////////////////////
	fclose(f);
	return 1;
}
//-------------------------------------------------------------------------------------
void displayDigitList(digitList* L)
{
	if (L->nextDigit == NULL) cout << L->digit;
	else
	{
		displayDigitList(L->nextDigit);
		cout << L->digit;
	}
}
//--------------------------------------------------------------------------------------
void displayInteger(Integer A)
{
	if (A.sign<0) cout << "-";
	displayDigitList(A.digits);
}
//---------------------------------------------------------------------------------------
bool checkInteger(Integer A, Integer B)
{
	if (A.sign != B.sign) return false;
	if (A.length() != B.length()) return false;
	for (int i = 0; i < A.length(); i++)
	{
		if (A.digits->digit != B.digits->digit) return false;
		A.digits = A.digits->nextDigit;
		B.digits = B.digits->nextDigit;
	}
	return true;
}
//---------------------------------------------------------------------------------------
void display(Integer A, Integer B, int testcase)
{
	//sprintf(student, "%.2f", fOut_Student);
	bool chRight = checkInteger(A, B);
	if (!chRight)
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	else
	    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);

	cout << "TC " << testcase << "       " << endl;
	displayInteger(A);
	cout << endl;
	cout << "";
	displayInteger(B);
	cout << "";
	cout << endl;
	if (!chRight)
	{
		cout << "----------------->FAIL\n";
		rStack.push(testcase);
		countFail ++;
	}
	else
	{
		fStack.push(testcase);
		countPass++;
	}
}
//-------------------------------------------------------
int read_OutputFile(int i, Integer& A)
{
	char s[30];
	char* filename = itoa_Vinh(i, s, 10, 2);
	FILE* f = 0;
	f = fopen(filename, "r");
	if (f == NULL)
	{
		cout << "Ko tim thay file output" << endl;
		return 0;
	}
	char str[300];
	fgets(str, 300, f);
	A = Integer(str);
	return 1;
}

void displayStack(stack<int> st)
{
	if (st.empty())
	{
		cout << "Khong co testcase sai";
		return;
	}
	while(!st.empty())
	{
		cout << st.top() <<  ' ';
		st.pop();
	}
	cout << endl;
}

int main(int argc, char* argv[])
{
	cout << "==================================================================="<<endl;
	cout << "                      TESTING RESULT                              "<<endl;
	cout << "		  ASSIGNMENT? NO PROBLEM								   "<<endl;
	cout << "		I THINK YOU CAN HANDLE THAT								   "<<endl;
	cout << "==================================================================="<<endl;
	int		operandNum;
	int		operatorNum;
	char* filename;
	char s[30];
	int	i = 0;
	clock_t begin_tc;
	clock_t end_tc;
	double time_run_tc;
	for (int i = 1; i <= NUMBER_OF_TESTCASE; i++)
	{
		filename = itoa_Vinh(i, s, 10, 1);
		if (readFile(filename, operandNum, operatorNum) == 0)
		{
			cout << "Read data file faild";
			return 0;
		}
		Integer L1;
		begin_tc = clock();
		L1 = computeValue(operatorNum);
		end_tc = clock();
		time_run_tc = (double)(end_tc - begin_tc) / CLOCKS_PER_SEC;
		Integer KQ;
		read_OutputFile(i, KQ);
		display(KQ, L1, i);
		cout << "Time: " << time_run_tc << "s";
		if (time_run_tc > 3)
			cout << " ---> TIME FAILED" << endl;
		cout << endl;
		cout << endl;
	}
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "Wrong: " << countFail << endl;
	cout << "->";
	displayStack(rStack);
	cout << endl;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << "Right: " << countPass << endl;
	cout << endl;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
	cout << "		   Copyright: Nguyen Van Huy & Tran Van Tien" << endl;
	cout << endl;
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	system("pause");
	return 0;
}
