//#include "common.h"

#ifndef _common_h_
#include "common.h"
#define _common_h_
#endif

Integer factorial(Integer L) {
	if (trimDigitList(L.digits) == NULL) {
		return Integer(1);
	}
	if (L.digits->digit == 1 && L.digits->nextDigit == NULL) {
		return L;
	}
	return L * factorial(L - Integer(1));
}

Integer digitPower(Integer L, Integer L2, Integer L3) {
	if (trimDigitList(L2.digits) == NULL) {
		return Integer(1);
	}
	if (L2.digits->digit == 1 && L2.digits->nextDigit == NULL) {
		return L;
	}
	L3 = L3.trimDigit();
	L = L * L3;
	L2 = L2 - Integer(1);
	return digitPower(L, L2, L3);
}

digitList *digitize(char str[80]) {
	digitList* L = 0; 
	digitList* L2 = 0;
	digitList* node;
	for(int i = 0; i < strlen(str); i++) {
		if (str[i] < '0' || str[i] > '9') {
			if (str[i] == '!') {
				return factorial(L).digits;
			}
			else if (str[i] == '^') {
				for (int j = i + 1; j < strlen(str); j++) {
					if (str[j] < '0' || str[j] > '9')
						break;
					node = new digitList(str[j] - '0', L2);
					L2 = node;
				}
				return digitPower(trimDigitList(L), trimDigitList(L2), trimDigitList(L)).digits;
			}
			break;
		}
		node = new digitList(str[i] - '0', L);
		L = node;
	}
	return L;
}

digitList *trimDigitList(digitList* L) {
	if (L == NULL) {
		return NULL;
	}
	L->nextDigit = trimDigitList(L->nextDigit);
	if (L->nextDigit == NULL) {
		if (L->digit == 0) {
			free(L);
			return NULL;
		}
		else {
			return L;
		}
	}
	return L;
}

digitList *subDigitLists(int b, digitList* L1, digitList* L2) {
	if ((L1 == NULL) && (L2 == NULL)) {
		return digitize(b);
	}
	else if (L1 == NULL) {
		return subDigitLists(b, L2, NULL);
	}
	else if (L2 == NULL) {
		int t = L1->getDigit() - b;
		if (t < 0) {
			t += 10;
			return new digitList(t % radix, subDigitLists(1, L1->getNextDigit(), NULL));
		}
		else {
			return new digitList(t % radix, subDigitLists(t / radix, L1->getNextDigit(), NULL));
		}
	}
	else {
		int t = L1->getDigit() - L2->getDigit() - b;
		if (t < 0) {
			t += 10;
			return new digitList(t % radix, subDigitLists(1, L1->getNextDigit(), L2->getNextDigit()));
		}
		else {
			return new digitList(t % radix, subDigitLists(t / radix, L1->getNextDigit(), L2->getNextDigit()));
		}
	}
}

Integer computeValue(int operatorNum) {
	Integer		L1, L2;
	L1 = operandArr[0];
	for (int i = 0; i < operatorNum; i++) {
		L2 = operandArr[i + 1];
		switch (operatorArr[i]) {
		case '+':
			L1 = L1 + L2;
			break;
		case '-':
			L1 = L1 - L2;
			break;
		case '*':
			L1 = L1 * L2;
			break;
		}
	}
	return L1;
}

Integer Integer::operator +(Integer L) {
	digitList* a = trimDigitList(digits);
	digitList* b = trimDigitList(L.digits);
	switch (compareDigitLists(a, b)) {
	case 0:
		if (sign == L.sign) {
			return Integer(sign, addDigitLists(0, a, b)).trimDigit();
		}
		else {
			return Integer(1, new digitList(0, NULL));
		}
	case 1:
		if (sign == L.sign) {
			return Integer(sign, addDigitLists(0, a, b)).trimDigit();
		}
		else {
			return Integer(sign, subDigitLists(0, a, b)).trimDigit();
		}
	case -1:
		if (sign == L.sign) {
			return Integer(sign, addDigitLists(0, a, b)).trimDigit();
		}
		else {
			return Integer(-sign, subDigitLists(0, b, a)).trimDigit();
		}
	}
	return Integer(0);
}

Integer Integer::operator -(Integer L) {
	digitList* a = trimDigitList(digits);
	digitList* b = trimDigitList(L.digits);
	switch (compareDigitLists(a, b)) {
	case 0:
		if (sign == L.sign) {
			return Integer(1, new digitList(0, NULL));
		}
		else {
			return Integer(sign, addDigitLists(0, a, b)).trimDigit();
		}
	case 1:
		if (sign == L.sign) {
			return Integer(sign, subDigitLists(0, a, b)).trimDigit();
		}
		else {
			return Integer(sign, addDigitLists(0, a, b)).trimDigit();
		}
	case -1:
		if (sign == L.sign) {
			return Integer(-sign, subDigitLists(0, b, a)).trimDigit();
		}
		else {
			return Integer(sign, addDigitLists(0, a, b)).trimDigit();
		}
	}
	return Integer(0);
}

Integer Integer::leftDigits(int n) {
	if (n < 1 || n >= length()) {
		return Integer(0);
	}
	else if (digits->nextDigit == NULL) {
		return Integer(1, digits);
	}
	else return Integer(1, digits->leftDigits(n));
}

Integer Integer::rightDigits(int n) {
	if (n < 1 || n >= length()) {
		return Integer(0);
	}
	else return Integer(1, digits->rightDigits(n));
}

Integer	Integer::shift(int n) {
	if (digits == NULL) {
		return Integer(0);
	}
	if (n < 1) {
		return Integer(sign, digits);
	}
	else {
		digitList* temp = new digitList(0, digits);
		digits = temp;
		return this->shift(n - 1);
	}
}

void add0Last(digitList* x, int n) {
	if (n < 1) {
		return;
	}
	else {
		digitList* last = x;
		while (last->nextDigit) {
			last = last->nextDigit;
		}
		last->nextDigit = new digitList();
		add0Last(x, n - 1);
	}
}

Integer Integer::operator *(Integer Y) {
	digits = trimDigitList(digits);
	Y.digits = trimDigitList(Y.digits);
	if (length() == 0 || Y.length() == 0) {
		return  Integer(1, new digitList());
	}
	if (length() == 1 && Y.length() == 1) {
		return Integer(1, digitize(digits->digit * Y.digits->digit));
	}
	if (length() < Y.length()) {
		add0Last(digits, Y.length() - length());
	}
	else if (length() > Y.length()) {
		add0Last(Y.digits, length() - Y.length());
	}
	int n = length() / 2;
	int m = length() - n;
	Integer result = leftDigits(n) * Y.leftDigits(n) +
		(leftDigits(n) * Y.rightDigits(m) + rightDigits(m) * Y.leftDigits(n)).shift(n) +
		(rightDigits(m) * Y.rightDigits(m)).shift(2 * n);
	result.sign = sign * Y.sign;
	return result;
}
