#include <iostream>
using namespace std;

//Selection Sort
void selectionSort(int array[], int n) {
	int curr, min, run;
	for (curr = 0; curr < n - 1; curr++) {
		min = curr;
		run = curr + 1;
		for (run; run < n; run++) {
			if (array[run] < array[min]) {
				min = run;
			}
		}
		swap(array[curr], array[min]);
	}
}

//Insertion Sort
void insertionSort(int array[], int n) {
	int j;
	for (int i = 1; i < n; i++) {
		j = i;
		while (j > 0 && array[j] < array[j - 1]) {
			swap(array[j], array[j - 1]);
			j--;
		}
	}
}

//Bubble Sort
void bubbleSort(int array[], int n) {
	int curr = 0;
	bool flag = false;
	while (curr < n && flag == false) {
		int run = n - 1;
		flag = true;
		for (run; run > curr; run--) {
			if (array[run] < array[run - 1]) {
				swap(array[run], array[run - 1]);
				flag = false;
			}
		}
		curr++;
	}
}

int main() {
	int array[9] = { 3, 7, 4, 6, 15, 11, 5, 10, 14 };
	selectionSort(array, 9);
	// Print array to the screen
	for (int i = 0; i < 9; i++) {
		cout << array[i] << " ";
	}
	cout << endl;
	insertionSort(array, 9);
	for (int i = 0; i < 9; i++) {
		cout << array[i] << " ";
	}
	cout << endl;
	bubbleSort(array, 9);
	for (int i = 0; i < 9; i++) {
		cout << array[i] << " ";
	}
	return 0;
}