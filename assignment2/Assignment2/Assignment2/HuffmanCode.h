#ifndef _HUFFMAN_CODE_H_
#define _HUFFMAN_CODE_H_

#include <iostream>
#include <fstream>
#include <cstring>

#include "Reader.h"
#include "Writer.h"
#include "HCZHeader.h"

using namespace std;

#define ERR -1
#define SUCCESS 0
#define UN_IMPLEMENT -2
#define MAXBUFSIZE 10000

// convert byte to character
#define byte2Char(val) ((unsigned char) ((val) % 256))

// convert character to ASCII
#define char2Int(ch) ((int) (ch) < 0 ? 128 + (((int) (ch)) & 127): (((int) (ch)) & 127))

struct Node {
	char data;
	int frequency;
	Node* next;
	Node* left;
	Node* right;

	Node() {
		data = frequency = 0;
		next = left = right = NULL;
	}

	Node(char data, int frequency) {
		this->data = data;
		this->frequency = frequency;
		next = left = right = NULL;
	}

	Node(char data, int frequency, Node* next, Node* left, Node* right) {
		this->data = data;
		this->frequency = frequency;
		this->next = next;
		this->left = left;
		this->right = right;
	}
};

class Queue {
private:
	Node* head;
	int count;
public:
	Queue();
	~Queue();
	Queue(const Queue&);
	bool enQueue(char data, int fre);
	bool deQueue();
	void sortingQueue(Node* head);
	void makeQueue(Queue& queue, int frequency[]);
	void push(Node*);
	int getCount();
	Node* getHead();
};

class HuffmanTree {
private:
	Node* root;
public:
	HuffmanTree();
	void deleteTree(Node* root);
	~HuffmanTree();
	Node* makeTree(Queue);
	bool makeBitCode(Node* head, char* bitcode, int& index, char c);
	Node* createLeaf(Node*, Node*);
	Node* getRoot();
	void setRoot(Node* root);
	void changeFreqToBit(Node* root, string leftOrRight);
	bool HuffmanTree::outputTree(Node* root, char data, int bitcodeIndex, char* bitcode, int index);
	bool getOriginalText(Node* root, string& originalText, char*& bitcode, int index, int bitcodeIndex, int textcodeIndex);
};

class HuffmanCode {
public:
    int zip(char* inputFile, char* outputFile);
    int unzip(char* inputFile, char* outputFile);
    HuffmanCode() {}
	void countFrequency(string str, int frequency[]);
	int getTotalBit(int frequency[], HuffmanTree& tree, char* bitcode, int& index);
	void setHCZ(int frequency[], HuffmanTree& tree, char* bitcode, HCZHeader* hcz, int& index);
	void makeTextcode(string str, HCZHeader* hcz, string& textcode, int& totalSize);

	void getTextcodeForUnzip(Reader* reader, int bodySize, string& textcode);
	void makeOutputTree(HCZHeader* hcz, HuffmanTree* tree, int& totalChar, int& index, char*& bitcode, string& str);
	void makeOriginalText(int& textcodeIndex, int& bitcodeIndex, int& originalTextIndex, int& bodySize, char*& bitcode, string& textcode, string& originalText, HuffmanTree* tree);
private:

};

#endif
