#ifndef _HUFFMAN_CODE_CPP_
#define _HUFFMAN_CODE_CPP_

#include "HuffmanCode.h"

// convert byte to character
#define byte2Char(val) ((unsigned char) ((val) % 256))

// convert character to ASCII
#define char2Int(ch) ((int) (ch) < 0 ? 128 + (((int) (ch)) & 127): (((int) (ch)) & 127))

int HuffmanCode::zip(char* inputFile, char* outputFile) {
 	HCZHeader* hcz = new HCZHeader();
	Reader reader(inputFile);
	Writer* writer = new Writer(outputFile);
	Queue queue;
	HuffmanTree tree;
	int frequency[128] = { 0 };
	char* bitcode = new char();
	int totalSize = 0;
	int index = -1;
	string str = "";
	string textcode = "";
	
	while (reader.isHasNext()) {
		str += reader.readChar();
	}

	if (str == "") {
		cout << "zipping...";
		return UN_IMPLEMENT;
	}

	countFrequency(str, frequency);
	queue.makeQueue(queue, frequency);
	queue.sortingQueue(queue.getHead());

	tree.setRoot(tree.makeTree(queue));
	tree.changeFreqToBit(tree.getRoot(), "none");

	hcz->setTotal(queue.getCount(), this->getTotalBit(frequency, tree, bitcode, index));
	this->setHCZ(frequency, tree, bitcode, hcz, index);
	this->makeTextcode(str, hcz, textcode, totalSize);
	hcz->setBodySize(totalSize);
	
	hcz->write(writer);

	for (int i = 0; i < totalSize; i++) {
		writer->writeByte(char2Int(textcode[i] - '0'));
	}

	writer->~Writer();
	cout << "zipping...";
	return UN_IMPLEMENT;
}

string byte2String(Reader* reader) {
	byte2Char(reader->readByte());
	byte2Char(reader->readByte());
	byte2Char(reader->readByte());
	reader->readDWord();
	int ret = reader->readWord();
	string temp;
	for (int i = 0; i < ret; ++i)
		temp += byte2Char(reader->readByte());
	return temp;
}

int HuffmanCode::unzip(char* inputFile, char* outputFile) {
	HCZHeader* hcz = new HCZHeader();
	Reader* reader = new Reader(inputFile);
	Writer writer(outputFile);
	HuffmanTree* tree = new HuffmanTree();
	tree->setRoot(new Node());

	hcz->read(reader);
	int bodySize = hcz->getBodySize();

	char* bitcode = new char();
	string str = byte2String(new Reader(inputFile));
	string textcode;
	string originalText;

	int totalChar = 0; 
	int totalbit = 0; 
	int originalTextIndex = 0;
	int bitcodeIndex = 0;
	int textcodeIndex = 0;
	int index = -1;
	
	hcz->getTotal(totalChar, totalbit);
	this->getTextcodeForUnzip(reader, bodySize, textcode);
	this->makeOutputTree(hcz, tree, totalChar, index, bitcode, str);
	this->makeOriginalText(textcodeIndex, bitcodeIndex, originalTextIndex, bodySize, bitcode, textcode, originalText, tree);

	for (int i = 0; i < originalTextIndex; i++) {
		writer.writeByte(char2Int(originalText[i]));
	}

	reader->~Reader();
	cout << "unzipping...";
    return UN_IMPLEMENT;
}

#pragma region QUEUE FUNCTIONS
Queue::Queue() {
	head = NULL;
	count = 0;
}

int Queue::getCount() {
	return count;
}

Node* Queue::getHead() {
	return head;
}

Queue::~Queue() {
	Node * temp;
	while (head != NULL) {
		temp = head;
		head = head->next;
		delete temp;
	}
	count = 0;
}

Queue::Queue(const Queue& queue) {
	Node* q1 = NULL;
	Node* q2 = NULL;

	if (queue.head == NULL) {
		head = NULL;
		count = 0;
	}
	else {
		head = new Node(queue.head->data, queue.head->frequency, NULL, NULL, NULL);
		q1 = head;
		q2 = queue.head->next;
	}
	while (q2) {
		q1->next = new Node(' ', 0, NULL, NULL, NULL);
		q1 = q1->next;
		q1->data = q2->data;
		q1->frequency = q2->frequency;
		q2 = q2->next;
	}
	q1->next = NULL;
	count = queue.count;
}

bool Queue::enQueue(char data, int fre) {
	Node* p = new Node(data, fre, NULL, NULL, NULL);
	if (head == NULL) {
		head = p;
	}
	else {
		Node* temp = head;
		while (temp->next) {
			temp = temp->next;
		}
		temp->next = p;
	}
	count++;
	return true;
}

bool Queue::deQueue() {
	if (head == NULL) {
		return false;
	}
	Node* p = head;
	head = head->next;
	delete p;
	count--;
	return true;
}

void Queue::push(Node* p) {
	if (head == NULL) {
		head = p;
	}
	else {
		p->next = head;
		head = p;
	}
	count++;
}

void Queue::sortingQueue(Node* head) {
	if (head->next == NULL) {
		return;
	}
	Node* temp = head->next;
	sortingQueue(temp);
	if (temp->frequency < head->frequency) {
		swap(temp->data, head->data);
		swap(temp->frequency, head->frequency);

		Node* tLeft = temp->left;
		temp->left = head->left;
		head->left = tLeft;

		Node* tRight = temp->right;
		temp->right = head->right;
		head->right = tRight;

		sortingQueue(head);
	}
	return;
}

void Queue::makeQueue(Queue& myQueue, int frequency[]) {
	for (int i = 0; i < 128; i++) {
		if (frequency[i] != 0) {
			myQueue.enQueue((char)i, frequency[i]);
		}
	}
}
#pragma endregion

#pragma region HUFFMANTREE FUNCTIONS
HuffmanTree::HuffmanTree() {
	root = NULL;
}

void HuffmanTree::deleteTree(Node* root) {
	if (root == NULL) {
		return;
	}
	if (root->left) {
		deleteTree(root->left);
	}
	else if (root->right) {
		deleteTree(root->right);
	}
	else delete root;
}

HuffmanTree::~HuffmanTree() {
	if (root != NULL) {
		deleteTree(root);
	}
	root = NULL;
}

Node* HuffmanTree::getRoot() {
	return root;
}

void HuffmanTree::setRoot(Node* root) {
	this->root = root;
}

Node* HuffmanTree::createLeaf(Node* a, Node* b) {
	return new Node(0, a->frequency + b->frequency, NULL, a, b);
}

Node* HuffmanTree::makeTree(Queue myQueue) {
	while (myQueue.getCount() != 1) {
		Node* a = new Node(myQueue.getHead()->data, myQueue.getHead()->frequency, NULL, myQueue.getHead()->left, myQueue.getHead()->right);
		myQueue.deQueue();
		Node* b = new Node(myQueue.getHead()->data, myQueue.getHead()->frequency, NULL, myQueue.getHead()->left, myQueue.getHead()->right);
		myQueue.deQueue();
		Node* c = createLeaf(a, b);
		myQueue.push(c);
		myQueue.sortingQueue(myQueue.getHead());
	}
	Node* result = new Node(myQueue.getHead()->data, myQueue.getHead()->frequency, NULL, myQueue.getHead()->left, myQueue.getHead()->right);
	return result;
}

void HuffmanTree::changeFreqToBit(Node* root, string leftOrRight) {
	if (root == NULL) return;
	if (leftOrRight == "left") {
		root->frequency = 0;
	}
	else if (leftOrRight == "right") {
		root->frequency = 1;
	}
	changeFreqToBit(root->left, "left");
	changeFreqToBit(root->right, "right");
	return;
}

bool HuffmanTree::makeBitCode(Node* head, char* bitcode, int &index, char c) {
	if (head == NULL) {
		index--;
		return false;
	}
	bitcode[index] = (char)(head->frequency + 48);
	if (makeBitCode(head->left, bitcode, ++index, c)) {
		return true;
	}
	if (makeBitCode(head->right, bitcode, ++index, c)) {
		return true;
	}
	if (head->data == c) {
		return true;
	}
	else {
		index--;
		return false;
	}
}

bool HuffmanTree::outputTree(Node* root, char data, int bitcodeIndex, char* bitcode, int index) {
	if (bitcode[bitcodeIndex] == '0') {
		if (bitcodeIndex == index - 1) {
			Node* node = new Node(data, 0);
			root->left = node;
			return true;
		}
		else {
			if (root->left == NULL) {
				Node* node = new Node(0, 0);
				root->left = node;
				outputTree(root->left, data, ++bitcodeIndex, bitcode, index);
			}
			else {
				outputTree(root->left, data, ++bitcodeIndex, bitcode, index);
			}
			return true;
		}
	}
	else {
		if (bitcodeIndex == index - 1) {
			Node* node = new Node(data, 1);
			root->right = node;
			return true;
		}
		else {
			if (root->right == NULL) {
				Node* node = new Node(0, 1);
				root->right = node;
				outputTree(root->right, data, ++bitcodeIndex, bitcode, index);
			}
			else {
				outputTree(root->right, data, ++bitcodeIndex, bitcode, index);
			}
			return true;
		}
	}
}

bool HuffmanTree::getOriginalText(Node* root, string& originalText, char*& bitcode, int index, int bitcodeIndex, int textcodeIndex) {
	if (root == NULL) {
		return false;
	}
	if (root->data != 0) {
		originalText += root->data;
		return true;
	}
	else if (index > bitcodeIndex) {
		return false;
	}
	else {
		if (bitcode[index] == '0') {
			if (getOriginalText(root->left, originalText, bitcode, ++index, bitcodeIndex, textcodeIndex)) {
				return true;
			}
		}
		else if (bitcode[index] == '1') {
			if (getOriginalText(root->right, originalText, bitcode, ++index, bitcodeIndex, textcodeIndex)) {
				return true;
			}
		}
	}
	return false;
}
#pragma endregion

#pragma region HUFFMANCODE FUNCTIONS
void HuffmanCode::countFrequency(string str, int frequency[]) {
	for (int i = 0; i < str.size(); i++) {
		frequency[(int)str[i]]++;
	}
}

int HuffmanCode::getTotalBit(int frequency[], HuffmanTree& tree, char* bitcode, int& index) {
	int totalBit = 0;
	for (int i = 0; i < 128; i++) {
		if (frequency[i] != 0) {
			tree.makeBitCode(tree.getRoot(), bitcode, index, (char)i);
			totalBit += ++index;
			index = -1;
		}
	}
	return totalBit;
}

void HuffmanCode::setHCZ(int frequency[], HuffmanTree& tree, char* bitcode, HCZHeader* hcz, int& index) {
	for (int i = 0; i < 128; i++) {
		if (frequency[i] != 0) {
			tree.makeBitCode(tree.getRoot(), bitcode, index, (char)i);
			bitcode[++index] = '\0';
			hcz->set((char)i, index, bitcode);
			index = -1;
		}
	}
}

void HuffmanCode::makeTextcode(string str, HCZHeader* hcz, string& textcode, int& totalSize) {
	int index = -1;
	char* bitcode = new char();
	for (int i = 0; i < str.size(); i++) {
		hcz->get(str[i], index, bitcode);
		for (int j = 0; j < index; j++) {
			textcode += bitcode[j];
			totalSize++;
		}
	}
}

void HuffmanCode::getTextcodeForUnzip(Reader* reader, int bodySize, string& textcode) {
	for (int i = 0; i < bodySize; i++) {
		textcode += byte2Char(reader->readByte() + '0');
	}
}

void HuffmanCode::makeOutputTree(HCZHeader* hcz, HuffmanTree* tree, int& totalChar, int& index, char*& bitcode, string& str) {
	for (int i = 0; i < totalChar; i++) {
		hcz->get(str[i], index, bitcode);
		tree->outputTree(tree->getRoot(), str[i], 0, bitcode, index);
	}
}

void HuffmanCode::makeOriginalText(int& textcodeIndex, int&bitcodeIndex, int& originalTextIndex, int& bodySize, char*& bitcode, string& textcode, string& originalText, HuffmanTree* tree) {
	while (textcodeIndex != bodySize) {
		bitcode[bitcodeIndex] = textcode[textcodeIndex];
		if (!tree->getOriginalText(tree->getRoot(), originalText, bitcode, 0, bitcodeIndex, originalTextIndex)) {
			++bitcodeIndex;
		}
		else {
			++originalTextIndex;
			bitcodeIndex = 0;
		}
		++textcodeIndex;
	}
}
#pragma endregion

#endif
